from django.urls import include, path

urlpatterns = [
    path('auth/', include('rest_auth.urls')),
    path('social/',include('app.social.urls')),
    path('users/', include('app.users.urls')),
    path('articles/', include('app.articles.urls')),
]
