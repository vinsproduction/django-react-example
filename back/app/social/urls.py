from django.urls import include, path
from .views import FacebookLogin, GoogleLogin, VkLogin


urlpatterns = [
    path('facebook/', FacebookLogin.as_view(), name='fb_login'),
    path('google/', GoogleLogin.as_view(), name='google_login'),
    path('vk/', VkLogin.as_view(), name='vk_login'),
]