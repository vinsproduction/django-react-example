from django.contrib import admin
from app.articles.models import Articles


class ArticlesAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'vote','created_at', 'updated_at')
    ordering = ('-created_at',)


admin.site.register(Articles, ArticlesAdmin)
