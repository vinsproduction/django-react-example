from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema

from app.articles.serializers import ArticlesSerializer
from app.articles.models import Articles


class ArticlesList(generics.ListAPIView):
    queryset = Articles.objects.all()
    serializer_class = ArticlesSerializer


class ArticlesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Articles.objects.all()
    serializer_class = ArticlesSerializer
    
    
class AddVoteSerializer(serializers.Serializer):
    vote = serializers.IntegerField()

@swagger_auto_schema(methods=['post'], request_body=AddVoteSerializer)
@api_view(['POST'])
def add_vote(request, pk):
    
    try:
        queryset = Articles.objects.get(id=pk)
        print(request.data)
        serializer = ArticlesSerializer(queryset, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response({"status": "error"}, status=status.HTTP_400_BAD_REQUEST)
    except:
        return Response({"status": "error", "message": "article not found"}, status=status.HTTP_404_NOT_FOUND)