from django.urls import path

from app.articles import views

urlpatterns = [
    path('', views.ArticlesList.as_view()),
    path('<int:pk>/', views.ArticlesDetail.as_view()),
    path('<int:pk>/add_vote/', views.add_vote),
]