from rest_framework import serializers
from app.articles import models


class ArticlesSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "title", "content", "vote", "created_at")
        model = models.Articles
