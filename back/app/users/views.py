from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import serializers
from django.core.serializers import serialize
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from app.users.serializers import UserSerializer

from django.contrib.auth import get_user_model
User = get_user_model()


@api_view()
def get_users(request):
    queryset = User.objects.all()
    serializer = UserSerializer(queryset, many=True)
    return Response(serializer.data)


@api_view()
def get_user_by_id(request, id):
    try:
        queryset = User.objects.get(id=id)
        serializer = UserSerializer(queryset)
        return Response(serializer.data)
    except:
        return Response({"status": "error", "message": "user not found"}, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_current_user(request):
    try:
        queryset = User.objects.get(id=request.user.id)
        serializer = UserSerializer(queryset)
        return Response(serializer.data)
    except:
        return Response({"status": "error", "message": "user not auth"}, status=status.HTTP_401_UNAUTHORIZED)



@swagger_auto_schema(methods=['patch'], request_body=UserSerializer)
@api_view(['PATCH'])
def update_current_user(request):
    queryset = User.objects.get(id=request.user.id)
    serializer = UserSerializer(queryset, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response({"status": "error"}, status=status.HTTP_400_BAD_REQUEST)
