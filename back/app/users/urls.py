from django.urls import path
from .models import User
from app.users import views

urlpatterns = [

    path('', views.get_users),
    path('current/', views.get_current_user),
    path('current/update', views.update_current_user),
    path('<int:id>/', views.get_user_by_id),
]
