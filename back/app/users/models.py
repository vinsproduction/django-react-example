import os
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


def path_and_rename(instance, filename):
    upload_to = settings.UPLOAD_DIR_AVATARS
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(instance.pk, ext)
    return os.path.join(upload_to, filename)


class User(AbstractUser):
    birth_date = models.DateField(null=True, blank=True)
    avatar = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
