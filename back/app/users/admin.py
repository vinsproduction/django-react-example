from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from django.contrib.auth import get_user_model
User = get_user_model()

class CustomUserAdmin(UserAdmin):
    model = User
    list_display = ('id', 'username', 'email', 'date_joined', 'last_login', 'is_superuser', 'is_staff', 'is_active',)
    list_display_links = ('username', )
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password', 'date_joined', 'last_login',)}),
        ('Personal', {'fields': ['birth_date', 'avatar']}),
        ('Permissions', {'fields': ('is_staff', 'is_superuser', 'is_active', 'groups', 'user_permissions',)}),
    )
    search_fields = ('email',)
    ordering = ('email',)


admin.site.register(User, CustomUserAdmin)
