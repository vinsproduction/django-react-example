import React, { useState, useEffect } from 'react';
import { ToastContainer } from 'react-toastify';

import useAuth from './store/auth';
import useLoader from './store/loader';
import useRouter from './store/router';

import Layout from './components/layout/Layout';
import Header from './components/header/Header';
import Error from './components/error/Error';
import Pages from './pages/Pages';
import Loader from './components/loader/Loader';

// Styles
import 'react-toastify/dist/ReactToastify.css';
import 'normalize.css';
import './app.styl';

const App: React.FunctionComponent = () => {

    const [pageError, setPageError] = useState(false);
    const [pageLoad, setPageLoad] = useState(false);

    const { startLoader } = useLoader();
    const { getAuth } = useAuth();
    const { route } = useRouter();

    useEffect(() => {

        (async () => {

            await startLoader();
            const [err] = await getAuth();

            if (err) {
                setPageError(true);
            } else {
                setPageLoad(true);
            }

        })();

    }, []);

    return (
        <React.Fragment>

            <Loader />

            <Layout>
                {route.name !== 'login' && <Header />}
                {pageError && <Error />}
                {pageLoad && <Pages />}
            </Layout>

            <ToastContainer
                position="top-right"
                autoClose={3000}
                hideProgressBar={true}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                draggable
                pauseOnHover
            />

        </React.Fragment>
    );
};

export default App;
