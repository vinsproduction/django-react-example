import React, { useState, useEffect } from 'react';

import Page from './../../components/layout/page/Page';

import useLoader from './../../store/loader';

import './404.styl';

import { block } from 'bem-cn';
const b = block('page-404');

const NotFound: React.FunctionComponent = () => {

    const [pageLoad, setPageLoad] = useState(false);
    const { stopLoader } = useLoader();

    useEffect(() => {

        (async () => {

            await stopLoader();
            setPageLoad(true);

        })();

    }, []);

    if (!pageLoad) {
        return null;
    }

    return (

        <Page center className={b()}>
            Страница не найдена
        </Page>

    );

};

export default NotFound;
