import React, { Suspense } from 'react';
import { Switch } from 'react-router-dom';
import routes from './../router/routes';
import Page from './Page';

const Pages: React.FunctionComponent = () => {

    return (
        <Suspense fallback={null}>
            <Switch>
                {
                    routes.map(route => (
                        <Page
                            key={route.name}
                            name={route.name}
                            path={route.path}
                            exact={route.exact}
                            component={route.component}
                            params={route.params}
                        />
                    ))
                }
            </Switch>
        </Suspense>
    );
};

export default Pages;
