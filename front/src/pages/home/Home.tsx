import React, { useState, useEffect } from 'react';

import Page from './../../components/layout/page/Page';
import Container from './../../components/layout/container/Container';
import Center from './../../components/layout/center/Center';
import Error from './../../components/error/Error';
import Button from './../../components/button/Button';

import useLoader from './../../store/loader';
import useArticles from './../../store/articles';

import './home.styl';

import { block } from 'bem-cn';
const b = block('page-home');

const Home: React.FunctionComponent = () => {

    const [pageError, setPageError] = useState(false);
    const [pageLoad, setPageLoad] = useState(false);
    const [articlesIndex, setArticlesIndex] = useState(0);

    const { stopLoader } = useLoader();
    const { articles, getArticles, addVote } = useArticles();

    const maxArticles = 3;

    const articlesPartial = articles.slice(articlesIndex, articlesIndex + maxArticles);

    useEffect(() => {

        (async () => {
            const [err] = await getArticles();

            await stopLoader();
            setPageLoad(true);

            if (err) {
                setPageError(true);
            }

        })();

    }, []);

    useEffect(() => {
        if (articlesIndex > 0 && !articlesPartial.length) {
            console.log('Статьи закончились', articles);
        }
    }, [articlesPartial, articlesIndex]);


    const nextArticles = () => {

        // Отравляем не выбранные как нейтральные
        articlesPartial.forEach(article => {
            if (!article.vote) {
                addVote(article.id, '0');
            }
        });

        setArticlesIndex(articlesIndex + maxArticles);
    }

    if (!pageLoad) {
        return null;
    }

    if (pageError) {
        return <Error />
    }

    return (

        <Page className={b()}>
            <Container>
                <Center>
                    <h1 className={b('title')}>Статьи</h1>

                    {!articlesPartial.length &&
                        <p>Статьи закончились</p>
                    }

                    {articlesPartial.length > 0 &&
                        <React.Fragment>
                            <div className={b('articles', { center: articlesPartial.length < 3 })}>
                                {articlesPartial.map(article => (
                                    <div key={article.id} className={b('article')}>

                                        <div className={b('article__inner')}>
                                            <div className={b('article__id')}>ID: {article.id}</div>
                                            <div className={b('article__title')}>{article.title}</div>
                                            <div className={b('article__text')}>{article.content}</div>
                                        </div>

                                        <div className={b('article__votes')}>
                                            <div
                                                className={b('article__vote', { like: true, active: article.vote === '1' })}
                                                onClick={() => addVote(article.id, '1')}
                                            ><span></span>
                                            </div>
                                            <div
                                                className={b('article__vote', { dislike: true, active: article.vote === '-1' })}
                                                onClick={() => addVote(article.id, '-1')}
                                            ><span></span>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            <div className={b('next')}>
                                <Button onClick={() => nextArticles()}>Далее</Button>
                            </div>
                        </React.Fragment>
                    }
                </Center>
            </Container>
        </Page>

    );

};

export default Home;
