import React from 'react';
import './container.styl';

import { block } from 'bem-cn';
const b = block('layout__container');

const Container: React.FunctionComponent<{className?: string}> = ({className = '', children}) => {

    return (
        <div className={b({}).mix(className)}>
            {children}
        </div>
    );

};

export default Container;
