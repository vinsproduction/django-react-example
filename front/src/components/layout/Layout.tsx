import React from 'react';

import './layout.styl';

import { block } from 'bem-cn';
const b = block('layout');

const Layout: React.FunctionComponent = ({ children }) => {

    return (
        <div className={b()}>

            <div className={b('pages')}>
                {children}
            </div>
        </div>
    );

};

export default Layout;
