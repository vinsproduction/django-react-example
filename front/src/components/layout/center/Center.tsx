import React from 'react';

import './center.styl';

import { block } from 'bem-cn';
const b = block('layout__center');

const Center: React.FunctionComponent<{className?: string}> = ({className = '', children}) => {

    return (
        <div className={b({}).mix(className)}>
            {children}
        </div>
    );
}

export default Center;
