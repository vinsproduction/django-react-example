import React from 'react';
import './field.styl';

import { block } from 'bem-cn';
const b = block('form-field');

const Field: React.FunctionComponent<{label: string; sublabel?: string; className?: string}> = ({ label, sublabel, children, className='' }) => {

    return (
        <label className={b({}).mix(className)}>
            <div className={b('label')}>
                {label} <span className={b('sublabel')}>{sublabel}</span>
            </div>
            <div className={b('input')}>
                {children}
            </div>
        </label>
    )

}

export default Field;
