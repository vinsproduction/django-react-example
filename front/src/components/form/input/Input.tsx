import React from 'react';

import './input.styl';

import { block } from 'bem-cn';
const b = block('form-input');

interface Props {
    style: any;
    className: string;
    type: string;
    name?: string;
    value: string | number;
    error: boolean;
    disabled: boolean;
    placeholder?: string;
    maxLength?: number;
    onClick: (e?: any) => any;
    onMouseDown: (e?: any) => any;
    onMouseUp: (e?: any) => any;
    onFocus: (e?: any) => any;
    onBlur: (e?: any) => any;
    onChange: (e?: any) => any;
    onKeyDown: (e?: any) => any;
    onKeyUp: (e?: any) => any;
    onKeyPress: (e?: any) => any;
    onCut: (e?: any) => any;
    onPaste: (e?: any) => any;
}


export default class Input extends React.Component<Props> {

    input: HTMLInputElement

    static defaultProps = {
        style: {},
        type: 'text',
        value: '',
        className: '',
        error: false,
        disabled: false,
        onClick: () => { },
        onMouseDown: () => { },
        onMouseUp: () => { },
        onFocus: () => { },
        onBlur: () => { },
        onChange: () => { },
        onKeyDown: () => { },
        onKeyUp: () => { },
        onKeyPress: ()=> { },
        onCut: () => { },
        onPaste: () => { },
    }

    state = {
        isFocused: false
    }

    render() {

        const classes = {
            error: this.props.error,
            focus: this.state.isFocused,
        }

        const props = {
            style: this.props.style,
            type: this.props.type,
            name: this.props.name,
            value: this.props.value,
            disabled: this.props.disabled,
            placeholder: this.props.placeholder,
            maxLength: this.props.maxLength,
            onClick: this.props.onClick,
            onMouseDown: this.props.onMouseDown,
            onMouseUp: this.props.onMouseUp,
            onFocus: this.handleFocus,
            onBlur: this.handleBlur,
            onChange: this.handleChange,
            onKeyDown: this.props.onKeyDown,
            onKeyUp: this.props.onKeyUp,
            onKeyPress: this.props.onKeyPress,
            onCut: this.props.onCut,
            onPaste: this.props.onPaste
        } as any as Props;

        return (
            <input
                ref={ref => this.input = ref}
                className={b(classes).mix(this.props.className)}
                {...props}
            />
        );
    }


    handleChange = e => {
        const target = e.target || e.currentTarget;
        this.props.onChange(target.value);
    }

    handleFocus = e => {
        this.setState({ isFocused: true });
        this.props.onFocus(e);
    }

    handleBlur = e => {
        this.setState({ isFocused: false });
        this.props.onBlur(e);
    }

}
