import React from 'react';

import './textarea.styl';

import { block } from 'bem-cn';
const b = block('form-textarea');

interface Props {
    style: object;
    className: string;
    name?: string;
    value: string | number;
    disabled?: boolean;
    onClick: (e?: any) => void;
    onFocus: (e?: any) => void;
    onBlur: (e?: any) => void;
    onChange: (e?: any) => void;
}


export default class Textarea extends React.Component<Props> {

    input: HTMLTextAreaElement

    static defaultProps = {
        style: {},
        value: '',
        className: '',
        disabled: false,
        onClick: () => { },
        onChange: () => { },
        onFocus: () => { },
        onBlur: () => { },

    }

    state = {
        isFocused: false
    }

    render() {

        const props = {
            style: this.props.style,
            name: this.props.name,
            value: this.props.value,
            className: b({ focus: this.state.isFocused }).mix(this.props.className),
            onClick: this.props.onClick,
            onChange: this.handleChange,
            onFocus: this.handleFocus,
            onBlur: this.handleBlur,
        } as any as Props


        return (
            <textarea
                ref={ref => this.input = ref}
                {...props}
            />
        );
    }


    handleChange = e => {
        const target = e.target || e.currentTarget;
        this.props.onChange(target.value);
    }

    handleFocus = e => {
        this.setState({ isFocused: true });
        this.props.onFocus(e);
    }

    handleBlur = e => {
        this.setState({ isFocused: false });
        this.props.onBlur(e);
    }

}
