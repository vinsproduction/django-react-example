import React from 'react';
import './submit.styl';

import { block } from 'bem-cn';
const b = block('form-submit');

interface Props {
    color?: 'none' | 'white';
    className?: string;
    style?: any;
    children: any;
}


export default class Submit extends React.Component<Props> {

    static defaultProps = {
        color: 'gray',
        style: {}
    }

    render() {

        const { className, style, color, children } = this.props;

        const classes = {
            color
        }

        const props = {
            type: 'submit',
            value: children,
            className: b(classes).mix(className),
            style
        }

        return (
            <input {...props} />
        );
    }
}

