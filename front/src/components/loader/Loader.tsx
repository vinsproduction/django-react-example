import React from 'react';

import useLoader from './../../store/loader';

import './loader.styl';

import { block } from 'bem-cn';
const b = block('loader');

const Loader: React.FunctionComponent = () => {

    const { loader } = useLoader();

    return (

        <div className={b({ active: loader.active })} >
            <div className={b('content')}>
                {loader.text}...
            </div>
        </div>
    );

};

export default Loader;
