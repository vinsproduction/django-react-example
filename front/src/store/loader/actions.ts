import { dispatch, getState } from './../store';
import { LOADER_DELAY } from './../../constants';
import { delay } from './../common';

import * as storeNames from './../storeNames';
import * as actionNames from './../actionNames';

export const startLoader = async (text = 'Загрузка'): Promise<void> => {

    const loader = getState(storeNames.LOADER);

    if (loader.active) {
        console.log('loader already run... skip');
        return;
    }

    dispatch({ type: actionNames.LOADER_START, payload: text });

    return;

};

export const stopLoader = async (timeout = 0): Promise<void> => {

    const loader = getState(storeNames.LOADER);

    if (!loader.active) {
        console.log('loader already stopped... skip');
        return;
    }

    const requestTime = new Date().getTime() - loader.time;
    if (requestTime < LOADER_DELAY) { timeout = LOADER_DELAY; }

    await delay(timeout);

    dispatch({ type: actionNames.LOADER_STOP });

    return;

};
