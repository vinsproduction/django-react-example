export const LOADER_START = 'LOADER_START';
export const LOADER_STOP = 'LOADER_STOP';

export const USER_IS_AUTH = 'USER_IS_AUTH';
export const USER_NOT_AUTH = 'USER_NOT_AUTH';

export const USER_LOGGED_IN = 'USER_LOGGED_IN';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';

export const ARTICLES_GET = 'ARTICLES_GET';
export const ARTICLES_ADD_VOTE = 'ARTICLES_ADD_VOTE';
