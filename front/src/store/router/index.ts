import * as storeNames from './../storeNames';
import useStore from './../useStore';
import state, { Route } from './state';
import * as actions from './actions';

const storeName = storeNames.ROUTE;

export {
    state,
    actions,
};

export default useStore<Route, typeof actions>(storeName, actions);
