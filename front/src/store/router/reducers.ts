import initialState from './state';
import { actionLog } from '../common';
import { LOCATION_CHANGE } from 'connected-react-router';
import routes from './../../router/routes';
import { getRouteByPath } from './../../router/util';

const Reducers = (state = initialState, action) => {

    const type = action.type;
    const payload = action.payload;

    if (type === LOCATION_CHANGE) {

        const path = payload.location.pathname;
        const route = getRouteByPath(routes, path);

        const newState = {
            from: state,
            path: path,
            query: route.match ? route.match.params : {},
            name: route.name,
            params: route.params,
        };

        actionLog('ROUTE_CHANGE', action, state, newState);
        return newState;
    }

    return state;
};

export default Reducers;
