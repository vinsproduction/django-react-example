import { Params } from './../../router/params';

export interface Route {
    from: Route;
    name: string;
    path: string;
    query: { [key: string]: string };
    params: Params;
}


const state: Route = null;

export default state;
