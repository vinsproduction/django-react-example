export interface Articles {
    id: number;
    title: string;
    content: string;
    vote?: '-1' | '0' | '1';
}

const state = [] as Articles[];

export default state;
