import initialState from './state';
import { actionLog } from '../common';

import * as actionNames from './../actionNames';

const Reducers = (state = initialState, action) => {

    let newState = state, defaultAction = false;
    const type = action.type;
    const payload = action.payload;

    switch (type) {
        case actionNames.ARTICLES_GET:
            newState = [...payload];
            break;
        case actionNames.ARTICLES_ADD_VOTE:
            newState = state.map(article =>
                article.id === payload.id ? { ...article, vote: payload.vote } : article
            );
            break;
        default:
            newState = state;
            defaultAction = true;
            break;
    }

    if (!defaultAction) {
        actionLog(type, action, state, newState);
    }
    return newState;
};

export default Reducers;
