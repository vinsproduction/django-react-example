import { dispatch } from './store';
import { redirect } from './router/actions';
import * as actionNames from './actionNames';

export const actionLog = (type: string, action: any, oldState: any, newState: any) => {

    console.log(`%c ${type} `, 'background: #9c9c9c; color: #FFF;', {
        action: { payload: action.payload, type: action.type },
        state: { old: oldState, new: newState }
    });
};

export const delay = (delay = 3000) => new Promise(resolve => setTimeout(resolve, delay));


export const apiErrorHandler = err => {

    if (err.response) {

        const { status } = err.response;

        if (status === 401) {
            dispatch({ type: actionNames.USER_NOT_AUTH });
            return;
        }
    }

    console.error('Api error', err.response || err);

    return err.response || err;

}
