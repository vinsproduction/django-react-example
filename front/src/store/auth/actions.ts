import axios from 'axios';
axios.defaults.xsrfHeaderName = "X-CSRFToken";

import { dispatch } from './../store';

import * as actionNames from './../actionNames';

import { Auth } from './state';

export const login = async (email: string, password: string): Promise<[any, any]> => {

    let data: any, err: any;

    try {

        data = await axios.post('/api/v1/auth/login/', { email, password }).then(res => res.data);
        dispatch({ type: actionNames.USER_LOGGED_IN });

    } catch (error) {
        err = error.response || error;
    }

    return [err, data];

};

export const logout = async (): Promise<[any, any]> => {

    let data: any, err: any;

    try {

        data = await axios.post('/api/v1/auth/logout/', {}).then(res => res.data);

        dispatch({ type: actionNames.USER_LOGGED_OUT });
        dispatch({ type: actionNames.USER_NOT_AUTH });

    } catch (error) {
        err = error.response || error;
    }

    return [err, data];

};


export const getAuth = async (): Promise<[any, Auth]> => {

    let user: Auth, err: any;

    try {

        const user = await axios.get('/api/v1/auth/user/').then(res => res.data);
        dispatch({ type: actionNames.USER_IS_AUTH, payload: user });

    } catch (error) {

        err = error.response || err;

        dispatch({ type: actionNames.USER_NOT_AUTH });

        // Не считаем кэтч ошибкой - пользователь не авторизован
        if (error.response && [401, 403].includes(error.response.status)) {
            err = null;
        } else {
            console.error(err);
        }

    }

    return [err, user];

};
