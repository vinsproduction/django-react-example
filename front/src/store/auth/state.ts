export interface Auth {
    id: number;
    email: string;
    name: string;

}

const state: Auth = null;

export default state;
