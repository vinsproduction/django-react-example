import initialState from './state';
import { actionLog } from '../common';

import * as actionNames from './../actionNames';

const Reducers = (state = initialState, action) => {

    let newState = state, defaultAction = false;
    const type = action.type;
    const payload = action.payload;

    switch (type) {
        case actionNames.USER_IS_AUTH:
            newState = payload;
            break;
        case actionNames.USER_NOT_AUTH:
            newState = null;
            break;
        case actionNames.USER_LOGGED_IN:
            break;
        case actionNames.USER_LOGGED_OUT:
            break;
        default:
            newState = state;
            defaultAction = true;
            break;
    }

    if (!defaultAction) {
        actionLog(type, action, state, newState);
    }
    return newState;
};

export default Reducers;
