import { createBrowserHistory } from 'history';
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';

import * as storeNames from './storeNames';

import { DEV_MODE } from './../constants';

import loader from './loader/reducers';
import auth from './auth/reducers';
import route from './router/reducers';
import articles from './articles/reducers';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    [storeNames.LOADER]: loader,
    [storeNames.AUTH]: auth,
    [storeNames.ROUTE]: route,
    [storeNames.ROUTER]: connectRouter(history),
    [storeNames.ARTICLES]: articles,
});

let composeEnhancer: typeof compose = compose;

if (DEV_MODE) {
    composeEnhancer = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ trace: true, traceLimit: 25 });
}

const store = createStore(
    rootReducer,
    composeEnhancer(
        applyMiddleware(
            routerMiddleware(history),
        )
    ),
);

export const dispatch = store.dispatch;

export const getState = storeName => store.getState()[storeName];

export default store;

