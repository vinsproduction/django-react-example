import React from 'react';
import params, { Params, PUBLIC, } from './params';

export interface Route {
    name: string;
    path?: string;
    exact?: boolean;
    component: any;
    match?: any;
    params?: Params;
}

export type Routes = Route[];

const routes: Routes = [
    {
        name: 'home',
        path: '/',
        exact: true,
        component: React.lazy(() => import('./../pages/home/Home')),
        params: {
            ...params,
            title: 'Рекомендации',
        },
    },
    {
        name: 'login',
        path: '/login',
        exact: true,
        component: React.lazy(() => import('./../pages/login/Login')),
        params: {
            ...params,
            title: 'Авторизация',
            access: PUBLIC,
        },
    },
    {
        name: '404',
        path: '/404',
        exact: true,
        component: React.lazy(() => import('./../pages/404/404')),
        params: {
            ...params,
            title: 'Страница не найдена',
            access: PUBLIC,
        },
    },
    {
        name: 'notFound',
        params: params,
        component: null,
    },

];

export default routes;
