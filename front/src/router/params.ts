export const PRIVATE = 'PRIVATE';
export const PUBLIC = 'PUBLIC';

export interface Params {
    access: string;
    title: string;
    wideScreen?: boolean;
    loader: {
        text: string;
    };
}

const params: Params = {
    access: PRIVATE,
    title: '',
    loader: {
        text: 'Загрузка',
    },
};

export default Object.freeze(params);
