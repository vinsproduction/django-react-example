import { matchPath } from 'react-router-dom';
import { Route } from './routes';

export const getRouteByPath = (routes: Route[], path: string): Route => {

    const findedRoute = routes.find(route => {
        const match = matchPath(path, { path: route.path, exact: true });
        route.match = match;
        return match;
    });

    return findedRoute || routes.find(route => route.name === 'notFound');
}
