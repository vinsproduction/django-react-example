### React starter

Dev server: http://127.0.0.1:9999

***

Install modules
```
npm ci
```

***

Build
```
npm run build
```

Dev server with proxy
```
npm run dev -- --env.proxy
```

Dev server with mock data
```
npm run dev
```
