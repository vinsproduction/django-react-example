### Back-end (https)

* https://127.0.0.1:8000/
* https://127.0.0.1:8000/admin
* https://127.0.0.1:8000/swagger

```
cd back

python3 -m venv env 

source env/bin/activate

pip install --upgrade pip

pip install -r requirements.txt

python manage.py makemigrations users
python manage.py makemigrations articles

python manage.py migrate

python manage.py createsuperuser

```

Run

```
python manage.py runserver_plus  --cert-file ./certs/server.cert --key-file ./certs/server.key
```

***

### Front-end

* http://127.0.0.1:9999


```
cd front
npm ci
npm run dev
```